from __future__ import print_function
import argparse
import torch
import torch.utils.data
from torch import nn, optim
from torch.nn import functional as F
from torchvision.utils import save_image
from typing import List, Callable, Union, Any, TypeVar, Tuple
from datetime import datetime
from torch.utils.tensorboard import SummaryWriter
import torchvision.utils as vutils
import numpy as np
import os

from data import dataset
from abc import abstractmethod
from vae.vanillaVae import VanillaVAE
from config import base_config

Tensor = TypeVar('torch.tensor')

MODEL_PATH = 'models'

args = base_config.config()
args.cuda = not args.no_cuda and torch.cuda.is_available()
torch.manual_seed(args.seed)

device = torch.device("cuda" if args.cuda else "cpu")


train_loader, test_loader = dataset.get_loader(args)

# model = VAE().to(device)
model = VanillaVAE(in_channels=1, latent_dim=args.latent_dim).to(device)
optimizer = optim.Adam(model.parameters(), lr=1e-3)
now = datetime.now()
writer = SummaryWriter(flush_secs=1, log_dir='checkpoints/' + now.strftime("%b%d_%H-%M-%S") + '/')
save_path = os.path.join(MODEL_PATH, now.strftime("%b%d_%H-%M-%S"))
os.makedirs(save_path, exist_ok=True)


def train(epoch):
    model.train()
    train_loss = 0
    for batch_idx, (data, _) in enumerate(train_loader):
        data = data.to(device)
        optimizer.zero_grad()
        recon_batch, input, mu, logvar = model(data)
        loss = model.loss_function(recon_batch, data, mu, logvar)
        loss.backward()
        # loss['loss'].backward()

        train_loss += loss.item()
        optimizer.step()
        if batch_idx % args.log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                       100. * batch_idx / len(train_loader),
                       loss.item() / len(data)))

    print('====> Epoch: {} Average loss: {:.4f}'.format(
        epoch, train_loss / len(train_loader.dataset)))
    return train_loss / len(train_loader.dataset)


def test(epoch):
    model.eval()
    test_loss = 0
    with torch.no_grad():
        for i, (data, _) in enumerate(test_loader):
            data = data.to(device)
            recon_batch, input, mu, logvar = model(data)

            test_loss += model.loss_function(recon_batch, data, mu, logvar).item()
            if i == 0 and epoch % 10 == 0:
                n = min(data.size(0), 8)
                comparison = torch.cat([data[:n],
                                        recon_batch[:n]])
                img = vutils.make_grid(comparison, padding=2, normalize=True)
                writer.add_images('reconstruction', img.cpu().numpy()[np.newaxis], epoch)


    test_loss /= len(test_loader.dataset)
    print('====> Test set loss: {:.4f}'.format(test_loss))
    return test_loss


def save_models(epoch):
    save_path_ = os.path.join(save_path, str(epoch) + '.pt')
    torch.save({
        'epoch': epoch,
        'model': model.state_dict(),
    }, save_path_)


if __name__ == "__main__":

    for epoch in range(1, args.epochs + 1):
        train_loss = train(epoch)
        writer.add_scalar('Loss/train_loss', train_loss, epoch)
        test_loss = test(epoch)
        writer.add_scalar('Loss/test_loss', test_loss, epoch)

        if epoch > 0 and epoch % args.plot_samples_step == 0:
            with torch.no_grad():
                sample = torch.randn(20, args.latent_dim).to(device)
                sample = model.decode(sample).cpu()
                img = vutils.make_grid(sample.view(20, 1, args.image_size, args.image_size), padding=2, normalize=True)
                writer.add_images('sample', img.numpy()[np.newaxis], epoch)
            if epoch > 0 and epoch % args.model_save_step == 0:
                save_models(epoch)

