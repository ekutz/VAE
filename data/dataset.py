import torch
import socket
from torchvision import datasets, transforms
import numpy as np

DEBUG = True if socket.gethostname().lower().startswith("Eikes-MacBook-Pro".lower()) else False
if DEBUG:
    DATAROOT_DIR_DRIVE = '/Users/eike/Datasets/DRIVE_masks/training'
else:
    DATAROOT_DIR_DRIVE = "/zhome/95/c/135723/Datasets/DRIVE_masks/training/"


def get_loader(args):
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

    if args.dataset == 'mnist':
        train_loader = torch.utils.data.DataLoader(
            datasets.MNIST('../data', train=True, download=True,
                           transform=transforms.Compose([transforms.Resize(args.image_size),
                                                         transforms.ToTensor()])),
            batch_size=args.batch_size, shuffle=True, **kwargs)
        test_loader = torch.utils.data.DataLoader(
            datasets.MNIST('../data', train=False, transform=transforms.Compose([transforms.Resize(args.image_size),
                                                                                 transforms.ToTensor()])),
            batch_size=args.batch_size, shuffle=True, **kwargs)

    elif args.dataset == 'drive' or args.dataset == 'gland':
        dataset_train = datasets.ImageFolder(root=args.dataroot,
                                             transform=transforms.Compose([
                                                 transforms.Grayscale(),
                                                 transforms.Resize(args.image_size),
                                                 transforms.CenterCrop(args.image_size),
                                                 transforms.ToTensor(),
                                                 lambda x: x > 0,
                                                 lambda x: x.float(),
                                                 transforms.Normalize([0.5], [0.5]),
                                             ]))
        dataset_test = datasets.ImageFolder(root=args.dataroot,
                                            transform=transforms.Compose([
                                                transforms.Grayscale(),
                                                transforms.Resize(args.image_size),
                                                transforms.CenterCrop(args.image_size),
                                                transforms.ToTensor(),
                                                lambda x: x > 0,
                                                lambda x: x.float(),
                                                transforms.Normalize([0.5], [0.5]),
                                            ]))
        ix = np.random.choice(len(dataset_train.imgs), len(dataset_train.imgs), False)
        tr, val = np.split(ix, [int(len(dataset_train.imgs) * 0.75)])  # set better values
        dataset_train.imgs = [dataset_train.imgs[i] for i in tr]
        dataset_train.targets = [dataset_train.targets[i] for i in tr]
        dataset_train.samples = [dataset_train.samples[i] for i in tr]
        dataset_test.imgs = [dataset_test.imgs[i] for i in val]
        dataset_test.targets = [dataset_test.targets[i] for i in val]
        dataset_test.samples = [dataset_test.samples[i] for i in val]
        train_loader = torch.utils.data.DataLoader(
            dataset_train,
            batch_size=args.batch_size, shuffle=True, **kwargs)
        test_loader = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=args.batch_size, shuffle=True, **kwargs)
    return train_loader, test_loader


def get_fake_loader(args):
    kwargs = {'num_workers': 1, 'pin_memory': True} if args.cuda else {}

    if args.dataset == 'drive' or args.dataset == 'gland':
        dataset = datasets.ImageFolder(root=args.fake_dataroot,

                                       transform=transforms.Compose([
                                           transforms.Grayscale(),
                                           transforms.Resize(args.image_size),
                                           transforms.CenterCrop(args.image_size),
                                           transforms.ToTensor(),
                                           lambda x: x > 0,
                                           lambda x: x.float(),
                                           transforms.Normalize([0.5], [0.5]),
                                       ]))
        train_loader = torch.utils.data.DataLoader(
            dataset,
            batch_size=args.batch_size, shuffle=True, **kwargs)

    return train_loader
