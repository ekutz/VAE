import argparse


def config():
    parser = argparse.ArgumentParser(description='VAE MNIST Example')
    parser.add_argument('--batch-size', type=int, default=20, metavar='N',
                        help='input batch size for training (default: 128)')
    parser.add_argument('--epochs', type=int, default=10, metavar='N',
                        help='number of epochs to train (default: 10)')
    parser.add_argument('--no-cuda', action='store_true', default=False,
                        help='enables CUDA training')
    parser.add_argument('--seed', type=int, default=1, metavar='S',
                        help='random seed (default: 1)')
    parser.add_argument('--log-interval', type=int, default=10, metavar='N',
                        help='how many batches to wait before logging training status')
    parser.add_argument('--dataset', type=str, default='mnist', choices=['mnist', 'drive', 'gland'])
    parser.add_argument('--dataroot', type=str)
    parser.add_argument('--latent_dim', type=int, default=256)
    parser.add_argument('--image_size', type=int, default=256)

    parser.add_argument('--model_path', type=str)
    parser.add_argument('--fake_dataroot', type=str)
    parser.add_argument('--model_save_step', type=int, default=None)
    parser.add_argument('--plot_samples_step', type=int, default=1)
    return parser.parse_args()
