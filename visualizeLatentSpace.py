import torch
import torch.utils.data
from torch import nn, optim
import os
import numpy as np
from sklearn import manifold, datasets, decomposition, discriminant_analysis
import matplotlib.pyplot as plt
from data import dataset
from config import base_config
from vae.vanillaVae import VanillaVAE
from visualization import plot_space
from sklearn.manifold import TSNE

SAVE_PATH = '/Users/eike/DTU/Evaluation/DisAnalysis/VAE'
os.makedirs(SAVE_PATH, exist_ok=True)
# if args.mode == 'train' or args.mode == 'optimize':
manualSeed = 999
# else:
#     manualSeed = random.randint(1, 10000)  # use if you want new results
print("Random Seed: ", manualSeed)
# random.seed(manualSeed)
np.random.seed(manualSeed)
torch.manual_seed(manualSeed)


def get_outliers(data, m=2.):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.
    return data[s > m]


if __name__ == '__main__':
    args = base_config.config()
    args.cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if args.cuda else "cpu")

    # load model
    model = VanillaVAE(in_channels=1, latent_dim=args.latent_dim).to(device)
    assert os.path.exists(args.model_path)
    if device.type == 'cpu':

        checkpoint = torch.load(args.model_path, map_location=lambda storage, location: storage)
    else:
        checkpoint = torch.load(self.args.model_path)
    model.load_state_dict(checkpoint['model'])
    model.eval()
    # load dataset
    train_loader_real, test_loader_real = dataset.get_loader(args)
    train_loader_fake = dataset.get_fake_loader(args)
    X_1 = np.concatenate([x.flatten(start_dim=1).numpy() for x, _ in train_loader_real], axis=0)
    X_0 = np.concatenate([x.flatten(start_dim=1).numpy() for x, _ in train_loader_fake], axis=0)
    print(X_0.shape)
    n_samples = len(X_0) + len(X_1)
    n_features = len(X_0[0]) + len(X_1[0])
    # tsne in pixel space
    pca = decomposition.PCA(n_components=2)
    tsne = TSNE(n_components=2, verbose=0, perplexity=45, n_iter=500)

    X_1_pca = pca.fit_transform(X_1)
    X_0_pca = pca.fit_transform(X_0)
    X_1_tsne = pca.fit_transform(X_1)
    X_0_tsne = pca.fit_transform(X_0)

    plot_space.embedding_plot(X_0_pca, X_1_pca, X_0, X_1, "DRIVE PCA-pixelspace", args)
    # plt.savefig(os.path.join(SAVE_PATH, args.dataset+'_PCA_pixelspace.png'), transparent=True)


    plot_space.embedding_plot(X_0_tsne, X_1_tsne, X_0, X_1, "DRIVE tsne-pixelspace", args)
    # plt.savefig(os.path.join(SAVE_PATH, args.dataset+'_TSNE_pixelspace.png'), transparent=True)

    # tsne in latent space
    X_1_latent = np.concatenate(
        [model.get_latent_space_representation(x).detach().numpy() for x, _ in train_loader_real], axis=0)
    X_0_latent = np.concatenate(
        [model.get_latent_space_representation(x).detach().numpy() for x, _ in train_loader_fake], axis=0)
    y = np.concatenate((np.array(len(X_1_latent) * [1]), np.array(len(X_0_latent) * [0])))

    X_1_pca = pca.fit_transform(X_1_latent)
    X_0_pca = pca.fit_transform(X_0_latent)
    plot_space.embedding_plot(X_0_pca, X_1_pca, X_0, X_1, "DRIVE PCA-latentspace", args)
    # plt.savefig(os.path.join(SAVE_PATH, args.dataset+'_PCA_latentspace.png'), transparent=True)
    plt.show()
    X = np.concatenate((X_0_latent, X_1_latent), axis=0)
    for perp in range(5,55,5):
        tsne = TSNE(n_components=2, verbose=0, perplexity=perp, n_iter=500)
        tsne_results = tsne.fit_transform(X)
        plot_space.embedding_plot_single(tsne_results, np.concatenate((X_0, X_1), axis=0), y, 't-sne', args)
        # plt.savefig(os.path.join(SAVE_PATH, args.dataset+'_TSNE-latentspace'+str(perp)+'.png'), transparent=True)
        # get outliers
        # plt.show()

    from time import time
    from sklearn.cluster import KMeans
    from sklearn import metrics
    from sklearn.decomposition import PCA
    from sklearn.preprocessing import scale

    print(X[0].dtype)
    X = np.float64(X)
    print(X[0].dtype)
    data = (X, y)
    data = scale(X)
    labels = y
    sample_size = 185


    def bench_k_means(estimator, name, data):
        t0 = time()
        estimator.fit(data)
        print('%-9s\t%.2fs\t%i\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f\t%.3f'
              % (name, (time() - t0), estimator.inertia_,
                 metrics.homogeneity_score(labels, estimator.labels_),
                 metrics.completeness_score(labels, estimator.labels_),
                 metrics.v_measure_score(labels, estimator.labels_),
                 metrics.adjusted_rand_score(labels, estimator.labels_),
                 metrics.adjusted_mutual_info_score(labels, estimator.labels_),
                 metrics.silhouette_score(data, estimator.labels_,
                                          metric='euclidean',
                                          sample_size=sample_size)))


    bench_k_means(KMeans(init='k-means++', n_clusters=2, n_init=10),
                  name="k-means++", data=data)

    bench_k_means(KMeans(init='random', n_clusters=2, n_init=10),
                  name="random", data=data)
    # X_1_pca = pca.fit_transform(X_1_latent)
    # X_0_pca = pca.fit_transform(X_0_latent)
    # plot_space.embedding_plot(X_0_pca, X_1_pca, X_0, X_1, "GLAND PCA-latentspace", args)
    # plt.savefig(os.path.join(SAVE_PATH, args.dataset + '_PCA_latentspace.png'), transparent=True)
    # plt.show()
    for perp in range(45, 50, 5):
        tsne = TSNE(n_components=2, verbose=0, perplexity=perp, n_iter=500, method='exact')
        tsne_results = tsne.fit_transform(X)

        plot_space.embedding_plot_single(tsne_results, np.concatenate((X_0, X_1), axis=0), y, 't-sne', args)
        # plt.savefig(os.path.join(SAVE_PATH, args.dataset + '_TSNE-discriminator_space'+str(perp)+'.png'), transparent=True)

    n_samples, n_features = data.shape
    n_digits = len(np.unique(y))
    labels = y

    sample_size = 300

    print("n_digits: %d, \t n_samples %d, \t n_features %d"
          % (n_digits, n_samples, n_features))

    print(82 * '_')
    print('init\t\ttime\tinertia\thomo\tcompl\tv-meas\tARI\tAMI\tsilhouette')

    bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10),
                  name="k-means++", data=data)

    bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10),
                  name="random", data=data)

    # in this case the seeding of the centers is deterministic, hence we run the
    # kmeans algorithm only once with n_init=1
    pca = PCA(n_components=n_digits).fit(data)
    bench_k_means(KMeans(init=pca.components_, n_clusters=n_digits, n_init=1),
                  name="PCA-based",
                  data=data)
    print(82 * '_')

    # #############################################################################
    # Visualize the results on PCA-reduced data

    reduced_data = PCA(n_components=2).fit_transform(data)
    kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
    kmeans.fit(reduced_data)

    # Step size of the mesh. Decrease to increase the quality of the VQ.
    h = .02  # point in the mesh [x_min, x_max]x[y_min, y_max].

    # Plot the decision boundary. For that, we will assign a color to each
    x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
    y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

    # Obtain labels for each point in mesh. Use last trained model.
    Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

    # Put the result into a color plot
    Z = Z.reshape(xx.shape)
    plt.figure(1)
    plt.clf()
    plt.imshow(Z, interpolation='nearest',
               extent=(xx.min(), xx.max(), yy.min(), yy.max()),
               cmap=plt.cm.Paired,
               aspect='auto', origin='lower')

    plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
    # Plot the centroids as a white X
    centroids = kmeans.cluster_centers_
    plt.scatter(centroids[:, 0], centroids[:, 1],
                marker='x', s=169, linewidths=3,
                color='w', zorder=10)
    plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
              'Centroids are marked with white cross')
    plt.xlim(x_min, x_max)
    plt.ylim(y_min, y_max)
    plt.xticks(())
    plt.yticks(())
    plt.show()
    print(y)
    print(kmeans.labels_)
    print(centroids)

    reduced_data = PCA(n_components=100).fit_transform(data)
    kmeans = KMeans(init='k-means++', n_clusters=n_digits, n_init=10)
    kmeans.fit(reduced_data)
    print(y)
    print(kmeans.labels_)
    print(kmeans.cluster_centers_)


